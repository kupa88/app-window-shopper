//
//  window_shopperTests.swift
//  window-shopperTests
//
//  Created by Trey Earnest on 7/29/17.
//  Copyright © 2017 Trey Earnest. All rights reserved.
//

import XCTest

class window_shopperTests: XCTestCase {
    
    //created test for logic to make sure the calculator rounds up
    func testGetHours() {
        XCTAssert(Wage.getHours(forWage: 25, andPrice: 100) == 4)
        XCTAssert(Wage.getHours(forWage: 15.50, andPrice: 250.53) == 17)
    }
}
