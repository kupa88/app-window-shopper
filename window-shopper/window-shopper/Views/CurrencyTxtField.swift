//
//  CurrencyTxtField.swift
//  window-shopper
//
//  Created by Trey Earnest on 7/29/17.
//  Copyright © 2017 Trey Earnest. All rights reserved.
//

import UIKit

//This IB command is going to configure IB to mimic runtime setup
@IBDesignable
class CurrencyTxtField: UITextField {
    // Created func for displaying the currency symbol in the wage and item text field
    override func draw(_ rect: CGRect) {
        let size: CGFloat = 20
        let currencyLbl = UILabel(frame: CGRect(x: 5, y: (frame.size.height / 2) - size / 2, width: size, height: size))
        // Setting currency label background color
        currencyLbl.backgroundColor = #colorLiteral(red: 0.7603307424, green: 0.7603307424, blue: 0.7603307424, alpha: 0.8)
        // Setting currency label text alignment
        currencyLbl.textAlignment = .center
        // Setting currency label text color
        currencyLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        // Setting currency label curved corner radius
        currencyLbl.layer.cornerRadius = 5.0
        // CLipping the image to bounds to keep the curved edges
        currencyLbl.clipsToBounds = true
        // Formatting numbers in the currency label
        let formatter = NumberFormatter()
        // Setting the number style to currency
        formatter.numberStyle = .currency
        // Formatting currency for the user's locale region
        formatter.locale = .current
        // Setting the locale sysbol for user currency
        currencyLbl.text = formatter.currencySymbol
        addSubview(currencyLbl)
    }
    
    // This func is requried to modify IB for runtime mimic
    override func prepareForInterfaceBuilder() {
        customizeView()
    }
    
    // created a custom class for resuable fields
    override func awakeFromNib() {
        super.awakeFromNib()
        customizeView()
    }
    func customizeView() {
        // set opacity amount for text field with colorLiteral
        backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.25)
        // rounded off the edges on the text field
        layer.cornerRadius = 5.0
        //aligned text to the center of the text field
        textAlignment = .center
        // Makes sure the edges are clipped in the custom view
        clipsToBounds = true
        
        // set the forground color for the text field, it nil it will run. If not it will make an attributed string
        if let p = placeholder {
            let place = NSAttributedString(string: p, attributes: [.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)])
            attributedPlaceholder = place
            textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
}
