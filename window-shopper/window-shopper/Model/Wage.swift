//
//  Wage.swift
//  window-shopper
//
//  Created by Trey Earnest on 7/29/17.
//  Copyright © 2017 Trey Earnest. All rights reserved.
//

import Foundation
// Created class and class function
class Wage {
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
}
