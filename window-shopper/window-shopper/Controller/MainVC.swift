//
//  ViewController.swift
//  window-shopper
//
//  Created by Trey Earnest on 7/29/17.
//  Copyright © 2017 Trey Earnest. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    // IBOutlet created for the wage text field
    @IBOutlet weak var wageTxt: CurrencyTxtField!
    // IBOutlet created for the price text field
    @IBOutlet weak var priceTxt: CurrencyTxtField!
    // IBOutlet created for hidding the results label
    @IBOutlet weak var resultLbl: UILabel!
    // IBOutlet created for hidding the hours label
    @IBOutlet weak var hoursLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // this constant is set for the calculate button at the bottom of the screen
        let calcBtn = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60))
        // this set's it's background color
        calcBtn.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        // this sets the calculators title
        calcBtn.setTitle("Calculate", for: .normal)
        // this sets the calculators title color
        calcBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        // #selector is an objective C command. will need a @objc in front of the func for this command
        calcBtn.addTarget(self, action: #selector(MainVC.calculate), for: .touchUpInside)
        
        // setting the button as the accessory view
        wageTxt.inputAccessoryView = calcBtn
        // setting the button as the accessory view
        priceTxt.inputAccessoryView = calcBtn
        
        resultLbl.isHidden = true
        hoursLbl.isHidden = true
    }
// the @objc is needed in swift 4 to run objective C commands
    @objc func calculate() {
        // set new constant to match private var for wageTxt and priceTxt
        if let wageTxt = wageTxt.text, let priceTxt = priceTxt.text {
            if let wage = Double(wageTxt), let price = Double(priceTxt) {
                view.endEditing(true)
                // setting the buttons back to visible
                resultLbl.isHidden = false
                hoursLbl.isHidden = false
                // showing results for the equation
                resultLbl.text = "\(Wage.getHours(forWage: wage, andPrice: price))"
                
            }
        }
    }
// IBAction created to hide the results label, hours label, wage text field, and price text field
    @IBAction func clearCalculatorPressed(_ sender: Any) {
        resultLbl.isHidden = true
        hoursLbl.isHidden =  true
        wageTxt.text = ""
        priceTxt.text = ""
    }
    
}

